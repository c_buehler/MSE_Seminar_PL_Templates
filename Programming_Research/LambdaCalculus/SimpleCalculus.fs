﻿module LambdaCalculus.SimpleCalculus

    // λ(x,y).s | λx.λy.s | s = x+y
    let f = fun x -> fun y -> x+y;

    // λt.λf.t;
    let tru = fun t -> fun f -> t

    // λt.λf.f;
    let fls = fun t -> fun f -> f

    let c0 = fls
    let c1 = fun f -> fun x -> f x
    let c2 = fun f -> fun x -> f(f x)

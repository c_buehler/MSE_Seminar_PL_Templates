# Source

The template for the LaTeX article is from the ACM SIGPLAN (Special Interest Group on Programming Languages).

More information can be found on the following pages:

[http://www.sigplan.org/Resources/Author/](http://www.sigplan.org/Resources/Author/)  
[https://ctan.org/pkg/acmart](https://ctan.org/pkg/acmart)  

# Usage

This repository is configured to be usable via Eclipse, via the TeXclipse plugin. 

Alternatively, you may use your favourite test or LaTeX editor.

The included makefile can be used via the `make doc` command within the `Article` directory to build the pdf version of the article. The `make diff` command can be used to generate the diff pdf via latexdiff. Make sure that you tag the base version you want to use for the diff with the `latexdiff-base` tag in the repository. Take a look at `Article/makefile` for more information.

The repository has also been configured to automatically build `main.pdf` and `diff.pdf` upon every push onto gitlab. Take a look at `gitlab-ci.yml` for more information.

You may use the `releases` directory to store and commit individual versions of your article for convenience.

